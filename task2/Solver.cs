﻿using System;
using System.Collections.Generic;

namespace task2
{
    class Solver
    {
        static void Main(string[] args)
        {
            Dictionary<string, float> dictionary = new Dictionary<string, float>();
            string[] words = Console.ReadLine().Split(' ');
            float maxCount = 1;
            int maxLength = 1;
            int minLength = int.MaxValue;

            foreach (string word in words)
            {
                if (dictionary.ContainsKey(word))
                {
                    dictionary[word]++;
                    if (dictionary[word] > maxCount) maxCount = dictionary[word];
                }
                else
                {
                    dictionary[word] = 1;
                    if (word.Length > maxLength) { maxLength = word.Length; }
                    if (word.Length < minLength) { minLength = word.Length; }
                }
            }

            string dots = ".........."; //10 dots
            char[] indentArr = new char[maxLength - minLength];
            for (int i = 0; i < indentArr.Length; ++i)
            {
                indentArr[i] = '_';
            }
            string indent = new string(indentArr);
            

            foreach (var pair in dictionary)
            {
                int indentSize = maxLength - pair.Key.Length;
                int dotCount =(int)Math.Round((pair.Value / maxCount) * 10);
                Console.WriteLine($"{indent.Substring(0,indentSize)}{pair.Key} {dots.Substring(0, dotCount)}");
            }
        }
    }
}
