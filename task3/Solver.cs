using System;
using System.Collections.Generic;

namespace task3
{
    class Solver
    {
        const string operators = "+-*/";

        static void Main(string[] args)
        {
            Stack<string> expression = new Stack<string>(Console.ReadLine().Split(' '));
            Console.WriteLine(Calculate(expression));
        }

        static int Calculate(Stack<string> expression) 
        {
            string Operator = expression.Pop();
            if (!IsOperator(Operator[0])) return Convert.ToInt32(Operator);

            string operand = expression.Pop();
            int a;
            if (IsOperator(operand[0]))
            {
                expression.Push(operand);
                a = Calculate(expression);
            } else
            {
                a = Convert.ToInt32(operand);
            }

            operand = expression.Pop();
            int b;
            if (IsOperator(operand[0]))
            {
                expression.Push(operand);
                b = Calculate(expression);
            }
            else
            {
                b = Convert.ToInt32(operand);
            }

            switch (Operator)
            {
                case "/":
                    return b / a;
                case "*":
                    return b * a;
                case "+":
                    return b + a;
                case "-":
                    return b - a;
                default:
                    return 0; /*We can't be there*/
            }
            
        }

        static bool IsOperator(char c)
        {
            if (operators.IndexOf(c) != -1) return true;
            return false;
        }
    }
}
