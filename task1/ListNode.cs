﻿namespace task1
{
    class ListNode
    {
        public uint Value {
            get;
            private set;
        }
        public ListNode Next {
            get;
            private set;
        }

        /*Inserts unique values*/
        public void Insert(uint newValue)
        {
            ListNode node = this;
            while (node.Next != null)
            {
                if (node.Value == newValue) return;
                node = node.Next;
            }
            node.Value = newValue;
            node.Next = new ListNode();
        }
    }
}
