﻿namespace task1
{
    class HashTable
    {
        public ListNode[] values;

        public HashTable(byte n)
        {
            values = new ListNode[n];
        }

        public void Insert(uint newValue)
        {
            byte index =(byte)(newValue % values.Length);
            if (values[index] == null)
            {
                values[index] = new ListNode();

            }
            values[index].Insert(newValue);
        }

    }
}
