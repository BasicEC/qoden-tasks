﻿using System;

namespace task1
{
    class Solver
    {
        static void Main(string[] args)
        {
            byte N = byte.Parse(Console.ReadLine());
            HashTable table = new HashTable(N);
            string input = Console.ReadLine();
            string[] numbers = input.Split(' ');

            foreach (string num in numbers)
            {
                table.Insert(uint.Parse(num));
            }

            printHashTable(table);
        }

        static void printList(ListNode list)
        {
            while (list.Next != null)
            {
                Console.Write($"{list.Value} ");
                list = list.Next;
            }
        }

        static void printHashTable(HashTable table)
        {
            for (int i = 0; i < table.values.Length; ++i)
            {
                Console.Write($"{i}: ");
                if (table.values[i] != null) printList(table.values[i]);
                Console.WriteLine();
            }
        }
    }
}
